FROM node:12-slim
WORKDIR /usr/src/app
COPY package*.json ./
RUN npm install --only=production
RUN echo "👋 hello" 
RUN echo "👋 salut" 
COPY . ./
CMD [ "npm", "start" ]
